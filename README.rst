=====
SAILS
=====

SaILS is a free & open source Incident Learning System developed at The Ottawa Hospital Cancer Centre (TOHCC).

Some of the goals for SaILS are:

* Make it easier for everyone to report incidents electronically (web based)
* Make incident classification easy (causality, detection & origin domain)
* Track the completion of corrective/follow up actions
* Facilitate communication between RaMQIC members (email notification system)
* Easy bulk export of incidents for interoperability with other systems 
* Increase transparency (make real-time incident statistics visible to all staff)

SaILS is currently in a beta testing stage at TOHCC and still being actively developed.

SaILS is written in Python using the Django web framework.

Support Forum: http://groups.google.com/d/forum/sails-project 



