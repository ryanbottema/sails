from django.conf import settings

from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver



class ILSUser(AbstractUser):

    class Meta:
        db_table = 'auth_user'

    action_notifications = models.BooleanField(
        default=True,
        help_text="Subscribe to incident action notifications"
    )
    investigation_notifications = models.BooleanField(
        default=True,
        help_text="Subscribe to investigation assignment notifications"
    )

    def can_investigate(self):
        return self.has_perm("incidents.change_investigation")

