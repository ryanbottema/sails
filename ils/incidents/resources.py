import collections
import tablib
from django.contrib.comments.models import Comment
from django.contrib.contenttypes.models import ContentType
from import_export import resources
from import_export.fields import Field
from incidents import models


class IncidentResource(resources.ModelResource):


    incident_id = Field("id", "ID")
    psls_id = Field("investigation__psls_id", "PSLS ID")

    incident_date = Field("incident_date", "Incident Date")
    submitted = Field("submitted", "Date Reported")
    patient_id = Field("patient_id", "Patient ID")
    actual = Field("actual", "Actual/Potential")
    incident_type = Field("incident_type","Incident Type")
    severity = Field("severity", "Severity")
    technique = Field("technique__name", "Technique")
    location = Field("location__name", "Location")
    description = Field("description", "Description")
    investigator = Field("investigation__investigator", "Investigator")
    date_assigned = Field("investigation__assigned", "Date Assigned")
    notes = Field(column_name="Investigation Notes")
    origin_domain = Field(column_name="Origin Domain")
    detection_domain = Field(column_name="Detection Domain")
    standard_description = Field("standard_description", "Standard Description")
    intent = Field("intent", "Treatment Intent")
    harm = Field("investigation__harm", "Degree of Harm")
    cause = Field("investigation__cause__name", "Cause")
    operational_type = Field("investigation__operational_type__name", column_name="Operational Incident Type")
    completed = Field("investigation__completed", "Date Completed")

    action_type = Field(column_name="Action Type")
    action_responsible = Field(column_name="Responsible")
    action_assigned = Field(column_name="Assigned")
    action_completed = Field(column_name="Completed")
    action_priority = Field(column_name="Priority")
    action_escalation = Field(column_name="Escalation")
    action_description = Field(column_name="Description")

    class Meta:
        model = models.Incident
        fields = (
            "incident_id",
            "psls_id",
            "submitted",
            "incident_date",
            "location",
            "patient_id",
            "incident_type",
            "severity",
            "actual",
            "technique",
            "description",
            "investigator",
            "date_assigned",
            "notes",
            "detection_domain",
            "origin_domain",
            "standard_description",
            "intent",
            "harm",
            "cause",
            "operational_type",
            "completed",

            "action_type",
            "action_responsible",
            "action_assigned",
            "action_completed",
            "action_priority",
            "action_escalation",
            "action_description",
        )

        export_order = fields

    def __init__(self, *args, **kwargs):
        super(IncidentResource, self).__init__(*args, **kwargs)

        self.domain_cache = {}
        for parent in models.Domain.objects.filter(parent=None):
            for leaf in parent.get_leafnodes():
                self.domain_cache[leaf.pk] = leaf.ancestor_names_plain()


        all_comments = Comment.objects.filter(
            content_type = ContentType.objects.get_for_model(models.Investigation),
        ).select_related("user")

        self.comment_cache = collections.defaultdict(list)
        for c in all_comments:
            comment = "%s : %s : %s" %(c.submit_date, c.user, c.comment)
            self.comment_cache[c.object_pk].append(comment)

        all_actions = models.IncidentAction.objects.select_related(
            "incident__pk",
            "responsible",
            "action_type",
            "priority",
            "escalation"
        )

        self.actions_cache = collections.defaultdict(list)
        for ia in all_actions:
            self.actions_cache[ia.incident.pk].append(ia)


    def get_queryset(self, qs=None):
        if qs is None:
            qs = super(IncidentResource, self).get_queryset()

        return qs.select_related(
            "severity",
            "technique",
            "location",
            "standard_description"
            "investigation",
            "investigation__investigator",
            "investigation__origin_domain",
            "investigation__detection_domain",
            "investigation__operational_type",
            "investigation__cause",
        )


    def export(self, queryset=None):
        """
        Exports a resource.
        """
        queryset = self.get_queryset(queryset)
        headers = self.get_export_headers()
        data = tablib.Dataset(headers=headers)
        # Iterate without the queryset cache, to avoid wasting memory when
        # exporting large datasets.
        for obj in queryset:
            try:
                data.append(self.export_resource(obj))
            except:
                pass
        return data


    def dehydrate_incident_type(self, incident):
        return incident.get_incident_type_display()

    def dehydrate_actual(self, incident):
        return incident.get_actual_display()

    def dehydrate_intent(self, incident):
        return incident.get_intent_display()

    def dehydrate_notes(self, incident):
        return "\n&&\n".join(self.comment_cache[incident.investigation.pk])

    def dehydrate_detection_domain(self, incident):
        d = incident.investigation.detection_domain
        return self.domain_cache[d.pk] if d else ""

    def dehydrate_origin_domain(self, incident):
        d = incident.investigation.origin_domain
        return self.domain_cache[d.pk] if d else ""

    def dehydrate_harm(self, incident):
        return incident.investigation.get_harm_display()

    def dehydrate_psls_id(self, incident):
        return incident.investigation.psls_id or ""

    def get_incident_actions(self, incident):
        return self.actions_cache[incident.pk]

    def dehydrate_action_type(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.action_type.name for ia in ias])

    def dehydrate_action_responsible(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.responsible for ia in ias])

    def dehydrate_action_assigned(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.assigned for ia in ias])

    def dehydrate_action_completed(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join(["%s" % ia.completed for ia in ias])

    def dehydrate_action_priority(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.priority.name if ia.priority else "N/A" for ia in ias])

    def dehydrate_action_escalation(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.escalation.name if ia.escalation else "N/A" for ia in ias])

    def dehydrate_action_description(self, incident):
        ias = self.get_incident_actions(incident)
        return "\n&&\n".join([ia.description for ia in ias])


class IncidentActionResource(resources.ModelResource):

    class Meta:
        model = models.IncidentAction
        fields = (
            'id',
            'incident',
            'action_type',
            'priority',
            'escalation',
            'assigned',
            'assigned_by',
            'complete',
            'completed',
            'responsible',
            'description'
        )
        export_order = fields

    def dehydrate_action_type(self, ia):
        return ia.action_type.name

    def dehydrate_responsible(self, ia):
        return ia.responsible.username

    def dehydrate_assigned_by(self, ia):
        return ia.assigned_by.username

    def dehydrate_complete(self, ia):
        return ["No", "Yes"][ia.complete]
