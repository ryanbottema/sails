import collections
import dateutil.parser
import json
import forms
from incidents import models
from notifications import signals
import re
import time

from django import http
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import login, get_user_model
from django.contrib.comments.models import Comment
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import connections
from django.db.models import Count, Q, Max
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import TemplateView, View, CreateView, DetailView, ListView, View
from django.views.generic.base import ContextMixin
from django.contrib import messages
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.template.defaultfilters import truncatechars
from django.utils import formats
from django.utils import timezone
from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator

from listable.views import Column as C, BaseListableView, SELECT, SELECT_ALL


class StaffRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm("incidents.change_incident"):
            raise PermissionDenied("You don't have authorization to view this page")
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            raise PermissionDenied("Please login before viewing this page")
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)



User = get_user_model()
MONTHS = [ "Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]


class RequestUserFormMixin(object):

    def get_form_kwargs(self):
        kwargs = super(RequestUserFormMixin, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs


class Report(RequestUserFormMixin, CreateView):

    model = models.Incident
    form_class = forms.IncidentReportForm

    def form_valid(self, form):
        self.get_form

        incident = form.instance

        incident.flag = settings.FLAG_ALL_ON_REPORT

        response = super(Report,self).form_valid(form)

        user = form.cleaned_data["submitted_by"]

        signals.incident_submitted.send(sender=self, incident=incident, user=user, subscribe=bool(form.cleaned_data["email"]))
        messages.success(self.request,"Thanks %s! Incident id=%d successfully reported" %(user.username, self.object.pk))
        return response

    def get_success_url(self):
        return reverse("report")

    def get_context_data(self, *args, **kwargs):
        print ("something fun is happening")
        context = super(Report,self).get_context_data(*args,**kwargs)
        severities = collections.defaultdict(list)
        for sdef in models.SeverityDefinition.objects.all():
            severities[sdef.incident_type].append({'id':sdef.severity, 'text':sdef.get_severity_display(), 'description':sdef.definition})

        context["severities"] = json.dumps(severities)

        return context


class CompleteIncident(StaffRequiredMixin, DetailView):

    model = models.Incident
    context_object_name = "incident"
    template_name = "incidents/complete_detail.html"

    def get_queryset(self):
        return models.Incident.objects.complete()


class Incident(StaffRequiredMixin, DetailView):

    model = models.Incident

    def post(self, request, *args, **kwargs):

        self.object = self.model.objects.get(pk=self.kwargs["pk"])

        context = self.get_context_data(**kwargs)
        context["object"] = self.object

        dispatch = {
            "investigation_form":self.handle_investigation_form,
            "incident_form": self.handle_incident_form,
            "actions_formset": self.handle_actions,
            "invalid_form": self.handle_invalid,
            "reopen_form": self.handle_reopen,
        }

        return dispatch[self.request.POST.get("form_name")](context)

    def get_queryset(self):
        return super(Incident, self).get_queryset().select_related(
            "investigation",
            "severity",
            "location",
            "cause"
        )

    def get_context_data(self, *args, **kwargs):

        context = super(Incident, self).get_context_data(*args,**kwargs)
        incident = self.model.objects.get(pk=self.kwargs["pk"])
        investigation = incident.investigation
        context["incident"] = incident
        context["investigation"] = investigation


        to_handle = self.request.POST.get("form_name")

        if to_handle == "investigation_form":
            context["investigation_form"] = forms.Investigation(self.request.POST, instance=investigation)
        else:
            context["investigation_form"] = forms.Investigation(instance=investigation)

        if to_handle == "incident_form":
            context["incident_form"] = forms.IncidentForm(self.request.POST, instance=incident)
        else:
            context["incident_form"] = forms.IncidentForm(instance=incident)

        if to_handle == "actions_formset":
            context["actions_formset"] = forms.IncidentActionFormSet(self.request.POST, queryset=models.IncidentAction.objects.filter(incident=self.object))
        else:
            context["actions_formset"] = forms.IncidentActionFormSet(queryset=models.IncidentAction.objects.filter(incident=self.object))

        for form in context["actions_formset"]:
            form.empty_permitted = False

        domain_roots = models.Domain.objects.filter(level=0).values_list("tree_id", flat=True)
        cause_roots = models.Cause.objects.filter(level=0).values_list("tree_id", flat=True)
        actions = models.ActionType.objects.order_by("strength").all()

        context["causes"] = [models.Cause.objects.filter(tree_id=tree_id) for tree_id in cause_roots]
        context["domains"] = [models.Domain.objects.filter(tree_id=tree_id) for tree_id in domain_roots]
        context["actions"] = [(s[1], models.ActionType.objects.filter(strength=s[0])) for s in models.STRENGTH_CHOICES]
        context["harms"] = [(h[0], h[1], models.DOH_DESCRIPTIONS[h[0]]) for h in models.DOH_CHOICES]
        context["severities"] = [(s.severity, s.get_severity_display(), s.definition)  for s in models.SeverityDefinition.objects.filter(incident_type=self.object.incident_type)]
        context["techniques"] = json.dumps(dict([(x.pk, x.modality) for x in models.Technique.objects.all()]))

        context['subscription'] = incident.subscriptions.filter(user=self.request.user).count() > 0

        context['edit_actions'] = bool(self.request.GET.get("edit-actions",False))

        return context

    def get_next_url(self):

        default = reverse("incident",kwargs={"pk":self.object.pk})

        next_ = self.request.GET.get("next",default)
        next_ = re.sub("^(.*)data\/?$","\g<1>",next_)

        next_from = self.request.GET.get("continue",None)

        if self.request.POST.get("save_and_continue","0") != "0":
            try:
                pk = getattr(models.Incident, next_from).exclude(pk=self.object.pk).values("pk").latest("pk")['pk']
                next_ = "%s?next=%s&continue=%s" %(reverse("incident",kwargs={"pk":pk}),next_, next_from)
            except (AttributeError, TypeError, models.Incident.DoesNotExist):
                pass

        edit_actions = self.request.GET.get("edit-actions", "")
        if edit_actions:
            if "?" in next_:
                next_ += "&edit-actions=%s" %(edit_actions)
            else:
                next_ += "?edit-actions=%s" %(edit_actions)

        return next_

    def handle_incident_form(self, context):

        if not self.request.user.has_perm("incidents.change_incident"):
            messages.error(self.request, "Error updating learning actions. You don't have the required permissions.")
            return self.render_to_response(context)

        form = context["incident_form"]

        if form.is_valid():

            inc = form.save()

            changed_to_actual = ("actual" in form.changed_data) and (inc.actual == models.ACTUAL)
            if changed_to_actual:
                inc.flag = True
                inc.investigation.save()

            comment = self.request.POST.get("comment","")
            if comment and comment.strip():
                self.create_comment(comment)

            messages.success(self.request, "Incident report successfully updated.")

            return HttpResponseRedirect(self.get_next_url())

        messages.error(self.request, "Error updating Incident. Please check form and try again.")

        return self.render_to_response(context)

    def handle_investigation_form(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)


        form = context["investigation_form"]

        if form.is_valid():

            inv = form.save(commit=False)
            prev_investigator = context["incident"].investigation.investigator
            cur_investigator = inv.investigator

            investigator_changed = "investigator" in form.changed_data
            if investigator_changed:
                inv.assigned_by = self.request.user
                inv.assigned = timezone.now()

            #changed_to_actual = ("actual" in form.changed_data) and (inv == models.ACTUAL)
            #if changed_to_actual:
            #    inv.flag = True

            needed_to_complete = set(inv.missing_fields())
            if needed_to_complete == set(["investigator"]):
                inv.investigator = self.request.user

            required_complete = inv.required_complete()
            just_completed = (not inv.complete) and required_complete

            inv.complete = required_complete

            if just_completed:
                inv.completed = timezone.now()

            if not required_complete:
                inv.completed = None

            inv.save()
            inv.incident.save()

            comment = self.request.POST.get("comment","")
            if comment and comment.strip():
                self.create_comment(comment)

            if investigator_changed and not required_complete:
                signals.investigator_assigned.send(sender=self, incident=inv.incident)
                if prev_investigator is not None and (prev_investigator != cur_investigator):
                    signals.investigator_unassigned.send(sender=self, incident=inv.incident, old_investigator=prev_investigator)


            if just_completed:
                signals.incident_completed.send(sender=self, incident=inv.incident)

            messages.success(self.request, "Investigation successfully updated." )

            return HttpResponseRedirect(self.get_next_url())

        messages.error(self.request, "Error updating Investigation. Please check form and try again." )

        return self.render_to_response(context)

    def handle_invalid(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)


        obj = context["object"]
        obj.valid = False
        obj.investigation.investigator = self.request.user
        obj.investigation.complete = True
        obj.investigation.completed = timezone.now().date()
        obj.save()
        obj.investigation.save()

        comment = self.request.POST.get("invalid-comment")
        comment = self.create_comment(comment)

        signals.incident_invalid.send(sender=self, incident=obj, comment=comment)
        messages.success(self.request, "Incident Marked as Invalid")
        return HttpResponseRedirect(reverse("incident",kwargs={"pk":self.object.pk}))

    def create_comment(self, comment):

        c = Comment(
            content_type = ContentType.objects.get_for_model(models.Investigation),
            object_pk    = self.object.investigation.pk,
            user = self.request.user,
            user_url     = "",
            comment      = comment,
            submit_date  = timezone.now(),
            site_id      = settings.SITE_ID,
            is_public    = True,
            is_removed   = False,
        )
        c.save()
        return c

    def handle_reopen(self, context):

        if not self.request.user.has_perm("incidents.change_investigation"):
            messages.error(self.request, "Error updating Incident. You don't have the required permissions.")
            return self.render_to_response(context)

        obj = context["object"]
        obj.valid = True
        obj.flag = True

        if obj.investigation.required_complete():
            obj.investigation.complete = True
            obj.investigation.completed = timezone.now()
        else:
            obj.investigation.complete = False
            obj.investigation.completed = None


        obj.investigation.save()
        obj.save()

        comment = self.request.POST.get("reopen-comment","")
        comment = self.create_comment(comment)

        signals.incident_reopened.send(sender=self, incident=obj, comment=comment)
        messages.success(self.request, "Incident Re-opened")
        return HttpResponseRedirect(reverse("incident",kwargs={"pk":self.object.pk}))

    def handle_actions(self, context):

        if not self.request.user.has_perms(["incidents.add_incidentaction", "incidents.change_incidentaction"]):
            messages.error(self.request, "Error updating learning actions. You don't have the required permissions.")
            return self.render_to_response(context)


        formset = context["actions_formset"]

        for form in formset:
            form.empty_permitted = True

        if formset.is_valid():
            changed = [f for f in formset if f.has_changed()]

            to_update = []
            for form in changed:
                prev_responsible = form.initial.get("responsible")
                if prev_responsible:
                    prev_responsible = User.objects.get(pk=prev_responsible)

                action = form.save(commit=False)
                cur_responsible = action.responsible

                responsible_changed = "responsible" in form.changed_data
                if responsible_changed:
                    action.assigned_by = self.request.user

                if "complete" in form.changed_data:
                    if form.cleaned_data['complete']:
                        action.completed = timezone.now()
                    else:
                        action.completed = None

                if not action.responsible:
                    action.responsible = self.request.user

                action.incident = self.object
                action.save()
                to_update.append(action)

                if responsible_changed:
                    signals.incident_action_assigned.send(sender=self, action=action)
                    if prev_responsible is not None and (prev_responsible != cur_responsible):
                        signals.incident_action_unassigned.send(sender=self, action=action, old_responsible=prev_responsible)



            if to_update:
                signals.incident_actions.send(sender=self, incident=self.object, actions=to_update)

            messages.success(self.request, "Learning actions successfully updated.")
            return HttpResponseRedirect(reverse("incident",kwargs={"pk":self.object.pk}))

        messages.error(self.request, "Error updating learning actions. Please check actions and try again.")

        return self.render_to_response(context)


class BaseIncidents(StaffRequiredMixin, BaseListableView):

    model = models.Incident

    columns = [
        C(field="flag", ordering="flag",  filtering=False),
        C(field="pk", ordering=True, filtering=True, header="ID"),
        C(field="submitted", ordering="submitted", filtering=False),
        C(field="incident_date", ordering="incident_date", filtering=False),
        C(field="completed", ordering="investigation__completed",  filtering=False),
        C(field="severity", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="actual", ordering="actual", filtering="actual", widget=SELECT_ALL),
        C(field="incident_type", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="description", ordering=False, filtering=True),
        C(field="investigator", ordering="investigation__investigator__username", filtering="investigation__investigator__username", widget=SELECT),
        C(field="actions",ordering="incidentaction__complete", filtering=False),
        C(field="view", ordering=False, filtering=False),
    ]

    def flag(self, obj):
        context = Context({"incident":obj})
        tmp = get_template('incidents/incident_flag.html')
        return tmp.render(context)

    def completed(self, obj):

        if obj.investigation.completed:
            txt = "%s" % formats.date_format(obj.investigation.completed, "SHORT_DATE_FORMAT")
            if not obj.valid:
                txt = '<abbr title="%s">Invalid</abbr>' % txt
        else:
            txt = ""
        return txt

    def submitted(self, obj):
        return formats.date_format(obj.submitted, "SHORT_DATE_FORMAT")

    def severity(self, obj):
        return obj.get_severity_display()

    def incident_type(self, obj):
        return obj.get_incident_type_display()

    def actual(self, obj):
        return obj.actual_display()

    def investigator(self, obj):
        if not obj.investigation.investigator:
            return ""
        return obj.investigation.investigator.username

    def description(self, obj):
        context = Context({"incident":obj})
        tmp = get_template('incidents/incident_description.html')
        return tmp.render(context)

    def actions(self, obj):
        actions = obj.incidentaction_set.all()
        complete = actions.filter(complete=True).count()
        total = actions.count()
        return '<abbr title="%d of %d corrective actions completed">%d/%d</abbr>' % (complete, total, complete, total)

    def get_save_and_continue(self, obj):
        return False

    def pk(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = RequestContext(self.request, {
            "incident":obj,
            "save_and_continue":save_and_continue,
            "display":obj.id,
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)

    def view(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = RequestContext(self.request, {
            "incident":obj,
            "save_and_continue":save_and_continue,
            "display":"View",
            "class":"btn btn-small",
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)


class Incidents(BaseIncidents):
    pass


class Complete(BaseIncidents):
    queryset = models.Incident.complete.all()
    template_name = "incidents/complete_list.html"


class Actual(BaseIncidents):
    queryset = models.Incident.actuals.all()
    template_name = "incidents/actual_list.html"

    columns = [
        C(field="flag", ordering="flag",  filtering=False),
        C(field="pk", ordering=True, filtering=True, header="ID"),
        C(field="submitted", ordering="submitted", filtering=False),
        C(field="incident_date", ordering="incident_date", filtering=False),
        C(field="completed", ordering="investigation__completed",  filtering=False),
        C(field="severity", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="psls_id", ordering="investigation__psls_id", filtering="investigation__psls_id" ),
        C(field="incident_type", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="description", ordering=False, filtering=True),
        C(field="investigator", ordering="investigation__investigator__username", filtering="investigation__investigator__username", widget=SELECT),
        C(field="actions",ordering="incidentaction__complete", filtering=False),
        C(field="view", ordering=False, filtering=False),
    ]

    def psls_id(self, obj):
        return obj.investigation.psls_id

class Incomplete(BaseIncidents):
    queryset = models.Incident.incomplete_or_flagged.all()
    template_name = "incidents/incomplete_list.html"


class Invalid(BaseIncidents):
    queryset = models.Incident.objects.filter(valid=False)
    template_name = "incidents/invalid_list.html"


class MyIncidents(BaseIncidents):
    template_name = "incidents/myincidents_list.html"

    def get_queryset(self):
        qs = super(MyIncidents, self).get_queryset()
        return qs.filter(submitted_by=self.request.user)

class Triage(BaseIncidents):
    queryset = models.Incident.triage.all()
    template_name = "incidents/triage_list.html"

    columns = [
        C(field="flag", ordering="investigation__flag",  filtering=False),
        C(field="pk", ordering=True, filtering=False, header="ID"),
        C(field="submitted", ordering="submitted", filtering=False),
        C(field="incident_date", ordering="incident_date", filtering=False),
        C(field="severity", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="incident_type", ordering=True, filtering=True, widget=SELECT_ALL),
        C(field="description", ordering=False, filtering=True),
        C(field="view", ordering=False, filtering=False),
    ]

    def get_save_and_continue(self, obj):
        return "triage"

    def pk(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = RequestContext(self.request, {
            "incident":obj,
            "save_and_continue":save_and_continue,
            "display":obj.id,
            "next":reverse("triage"),
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)
    def view(self, obj):
        save_and_continue = self.get_save_and_continue(obj)
        context = RequestContext(self.request, {
            "incident":obj,
            "save_and_continue":save_and_continue,
            "display":"View",
            "class":"btn btn-small",
            "next":reverse("triage"),
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)

class MyInvestigations(BaseIncidents):
    template_name = "incidents/myinvestigations_list.html"

    def get_queryset(self):
        qs = super(MyInvestigations, self).get_queryset()
        return qs.filter(investigation__investigator=self.request.user)


class BaseIncidentActionList(StaffRequiredMixin, BaseListableView):

    model = models.IncidentAction
    template_name = "incidents/incidentaction_list.html"

    columns = [
        C(field="pk", ordering=True, filtering=False, header="ID"),
        C(field="incident", ordering="incident__pk", filtering=False),
        C(field="assigned", ordering="assigned", filtering=False),
        C(field="responsible", ordering="responsible__username", filtering="responsible__username", widget=SELECT),
        C(field="completed", ordering="completed", filtering=False),
        C(field="action_type", ordering="action_type__name", filtering="action_type__name", widget=SELECT_ALL),
        C(field="priority", ordering="priority__name", filtering="priority__name", widget=SELECT_ALL),
        C(field="escalation", ordering="escalation__name", filtering="escalation__name", widget=SELECT_ALL),
        C(field="description", ordering=False, filtering=True),
        C(field="view", ordering=False, filtering=False),
    ]

    def get_queryset(self):
        qs = super(BaseIncidentActionList, self).get_queryset()
        qs = qs.select_related("incident", "action_type", "assignedby")
        return qs

    def assigned(self, obj):
        return formats.date_format(obj.assigned, "SHORT_DATE_FORMAT")

    def completed(self, obj):
        if obj.completed:
            return formats.date_format(obj.completed, "SHORT_DATE_FORMAT")
        return ""

    def responsible(self, obj):
        date = formats.date_format(obj.assigned, "SHORT_DATE_FORMAT")
        return '<abbr class="popoverDesc" title="Assigned by %s on %s" data-title="Action #%d">%s</abbr>' % (obj.assigned_by, date, obj.pk, obj.responsible)

    def description(self, obj):
        context = Context({"action":obj})
        tmp = get_template('incidents/action_description.html')
        return tmp.render(context)

    def escalation(self, obj):
        return "" if obj.escalation is None else obj.escalation.name

    def incident(self, obj):
        context = RequestContext(self.request, {
            "incident":obj.incident,
            "save_and_continue":"",
            "display":"%s" % obj.incident.pk,
            "class":"",
            "anchor":"#learning",
            "edit_actions":True
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)

    def view(self, obj):
        context = RequestContext(self.request, {
            "incident":obj.incident,
            "save_and_continue":"",
            "display":"View",
            "class":"btn btn-small",
            "anchor":"?edit#learning",
            "edit_actions":True
        })
        tmp = get_template('incidents/incident_link.html')
        return tmp.render(context)


class IncidentActions(BaseIncidentActionList):
    pass


class IncompleteIncidentActions(IncidentActions):
    template_name = "incidents/incidentaction_incomplete_list.html"

    def get_queryset(self):
        qs = super(IncompleteIncidentActions, self).get_queryset().filter(complete=False)
        return qs


class CompleteIncidentActions(IncidentActions):
    template_name = "incidents/incidentaction_complete_list.html"

    def get_queryset(self):
        qs = super(CompleteIncidentActions, self).get_queryset().filter(complete=True)
        return qs


class MyIncidentActions(BaseIncidentActionList):

    template_name = "incidents/incidentaction_mine_list.html"

    def get_queryset(self):
        qs = super(MyIncidentActions, self).get_queryset().filter(responsible=self.request.user)
        return qs


class MyIncompleteIncidentActions(MyIncidentActions):

    template_name = "incidents/incidentaction_mine_incomplete_list.html"

    def get_queryset(self):
        qs = super(MyIncompleteIncidentActions, self).get_queryset().filter(responsible=self.request.user, complete=False)
        return qs


class MyCompleteIncidentActions(MyIncidentActions):

    template_name = "incidents/incidentaction_mine_complete_list.html"

    def get_queryset(self):
        qs = super(MyCompleteIncidentActions, self).get_queryset().filter(responsible=self.request.user, complete=True)
        return qs


class Config(StaffRequiredMixin, TemplateView):

    template_name = "incidents/config.html"

    def get_context_data(self, *args, **kwargs):

        context = super(Config, self).get_context_data(*args, **kwargs)
        context["report_containers"] = [
            ("location","Locations", "choice"),
            ("severity","Incident Severities", "choice"),
            ("standarddescription","Standard Descriptions", "choice"),
        ]
        context["investigation_containers"] = [
            ("domain","Detection & Origin Domains","tree"),
            ("cause", "Causes", "tree"),
            ("priority","Priority Choices", "choice"),
            ("escalation","Escalation Choices", "choice"),
        ]
        return context


class Dashboard(StaffRequiredMixin, TemplateView):

    template_name = "incidents/dashboard.html"

    def post(self, request, *args, **kwargs):
        investigations = request.POST.get("investigations") == "on"
        actions = request.POST.get("actions") == "on"

        self.request.user.investigation_notifications = investigations
        self.request.user.action_notifications = actions
        self.request.user.save()
        messages.success(request, "Successfully updated your preferences")

        return HttpResponseRedirect(reverse("dashboard"))


    def add_incident_counts(self, context):

        from_ = timezone.now()-timezone.timedelta(days=365)


        now = timezone.now()
        last_year = now - timezone.timedelta(days=365)
        months = [time.localtime(time.mktime([now.year, now.month - n, 1, 0, 0, 0, 0, 0, 0]))[:2] for n in range(12)]
        months.reverse()


        def count_by_date(incidents):
            counts = []
            for year, month in months:
                inc = [x for x in incidents if x.incident_date.month == month and x.incident_date.year==year]
                counts.append(["%s '%s" % (MONTHS[month-1], str(year)[2:]), len(inc)])
            return counts

        def group_by(incidents, field, grouping):
            grouped_counts = {}
            for group_key, name  in grouping:
                grouped_counts[name] = count_by_date(incidents.filter(**{field:group_key}))
            return grouped_counts


        incidents = models.Incident.objects.filter(incident_date__gte=last_year)

        type_counts = group_by(incidents, "incident_type", models.INCIDENT_TYPE_CHOICES)

        severity_grouping = models.SEVERITY_CHOICES
        severity_counts = group_by(incidents, "severity", severity_grouping)

        status_counts = {
            "Actual": count_by_date(incidents.filter(actual=models.ACTUAL, valid=True)),
            "Potential": count_by_date(incidents.filter(actual=models.POTENTIAL, valid=True)),
            "Invalid": count_by_date(incidents.filter(valid=False)),
        }

        context["type_counts"] = json.dumps(type_counts)
        context["severity_counts"] = json.dumps(severity_counts)
        context["status_counts"] = json.dumps(status_counts)

    def add_summary_items(self, context):
        triage = models.Incident.triage.count()
        last_4_weeks = timezone.now()-timezone.timedelta(4*7)
        incs = models.Incident.objects.filter( submitted__gte=last_4_weeks)

        counts = collections.Counter()
        for inc in incs:
            counts[inc.incident_type] += 1

        type_counts = ["%d %s" % (cnt, dict(models.INCIDENT_TYPE_CHOICES)[itype],) for itype, cnt in counts.items()]

        items = [
            ("Triage", 'There are currently %d incidents awaiting <a href="%s">triage</a>' % (triage, reverse('triage'),)),
            ("Last 4 weeks:", ", ".join(type_counts)),
        ]
        context["summary_items"] = items

    def get_context_data(self, *args, **kwargs):
        context = super(Dashboard, self).get_context_data(*args, **kwargs)

        qs = models.Investigation.objects.user_incomplete(self.request.user)
        qs= qs.select_related("incident", "incident__severity")
        context['user_incomplete_investigations'] = qs

        incidentactions = models.IncidentAction.objects.filter(responsible=self.request.user, complete=False)
        incidentactions = incidentactions.select_related("action_type", "incident", "assigned_by")

        context["user_actions"] = incidentactions

        self.add_incident_counts(context)
        self.add_summary_items(context)

        return context


class IncompleteStats(StaffRequiredMixin, TemplateView):

    template_name = "incidents/incomplete_statistics.html"

    def get_context_data(self):
        context = super(IncompleteStats, self).get_context_data()

        year = timezone.now().year
        year_start = timezone.datetime(year=year, day=1, month=1)

        from_date = self.request.GET.get("from_date", None)
        from_date = dateutil.parser.parse(from_date) if from_date is not None else year_start

        to_date = self.request.GET.get("to_date", None)
        to_date = dateutil.parser.parse(to_date) if to_date is not None else timezone.now().date()

        incidents = models.Incident.objects.filter(
            incident_date__gte=from_date,
            incident_date__lte=to_date
        ).select_related(
            "investigation",
            "investigation__investigator"
        )

        incomplete_and_valid = incidents.filter(valid=True, investigation__complete=False)

        by_user = collections.defaultdict(list)
        for inc in incomplete_and_valid:
            by_user[inc.investigation.investigator].append(inc)

        by_user = dict(by_user)

        context.update({
            'from_date': from_date,
            'to_date': to_date,
            'all_incidents': incidents,
            'invalid': incidents.filter(valid=False),
            'complete_and_valid': incidents.filter(valid=True, investigation__complete=True),
            'incomplete_and_valid': incomplete_and_valid,
            'by_user': by_user
        })

        return context


class Statistics(LoginRequiredMixin, TemplateView):

    template_name = "incidents/statistics.html"

    def add_incident_counts(self, context, start, end):

        months = [time.localtime(time.mktime([start.year, n, 1, 0, 0, 0, 0, 0, 0]))[:2] for n in range(1,13)]

        def count_by_date(incidents):
            counts = []
            for year, month in months:
                inc = [x for x in incidents if x.incident_date.month == month and x.incident_date.year==year]
                counts.append(["%s '%s" % (MONTHS[month-1], str(year)[2:]), len(inc)])
            return counts

        def group_by(incidents, field, grouping):
            grouped_counts = {}
            grouped_counts2 = {}
            list_incidents = list(incidents)
            for group_key, name  in grouping:
                filtered = [x for x in list_incidents if getattr(x, field) == group_key]
                grouped_counts[name] = count_by_date(filtered)

            return grouped_counts


        incidents = models.Incident.objects.filter(incident_date__gte=start, incident_date__lt=end)

        type_counts = group_by(incidents, "incident_type", models.INCIDENT_TYPE_CHOICES)

        severity_grouping = models.SEVERITY_CHOICES
        severity_counts = group_by(incidents, "severity", severity_grouping)

        status_counts = {
            "Actual": count_by_date(incidents.filter(actual=models.ACTUAL, valid=True)),
            "Potential": count_by_date(incidents.filter(actual=models.POTENTIAL, valid=True)),
            "Invalid": count_by_date(incidents.filter(valid=False)),
        }

        context["type_counts"] = json.dumps(type_counts)
        context["severity_counts"] = json.dumps(severity_counts)
        context["status_counts"] = json.dumps(status_counts)

    def add_summary_table(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        )

        actuals = incidents.filter(actual=models.ACTUAL)
        potentials = incidents.filter(actual=models.POTENTIAL)
        invalid = models.Incident.objects.filter(
            valid=False,
            incident_date__gte=start,
            incident_date__lt=end
        )

        severities = [x[0] for x in models.SEVERITY_CHOICES]

        header = ["Status", "Type"] + [s[1] for s in models.SEVERITY_CHOICES] + ['Total']

        def tabulate_type(qs, name):
            list_qs = list(qs)
            totals = [len([x for x in list_qs if x.severity==s]) for s in severities]
            totals_row = [name, ''] + totals + [sum(totals)]
            table = [totals_row]
            for idx, (itype, name) in enumerate(models.INCIDENT_TYPE_CHOICES):
                sev_counts = [len([x for x in list_qs if x.incident_type==itype and x.severity==s]) for s in severities]
                row = ['', name] + sev_counts + [sum(sev_counts)]
                table.append(row)
            return table

        potential_table = tabulate_type(potentials, "Potential")
        actual_table = tabulate_type(actuals, "Actual")
        invalid_row = ["Invalid", ""]+['']*len(severities)+[len(invalid)]
        grand_totals = ["Grand Total",""]+ [incidents.filter(severity=s).count() for s in severities] + [incidents.exclude(severity=None).count()+invalid.count()]
        table = [header]+potential_table+actual_table+[invalid_row]+[grand_totals]

        context['summary_header'] = header
        context['actual_table'] = actual_table
        context['potential_table'] = potential_table
        context['invalid'] = invalid_row
        context['grand_totals'] = grand_totals

    def add_cause_summary(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        ).select_related("investigation__cause")
        incidents_list = list(incidents)

        max_depth = 1 + models.Cause.objects.aggregate(Max('level'))['level__max']

        table = []
        chart_data = []

        root_causes = models.Cause.objects.filter(parent=None)
        namesc = dict(models.Cause.objects.values_list("pk","name"))

        sub_causes = cache.get("sub_causes")
        if sub_causes is None:
            sub_causes = dict((r.pk, r.get_leafnodes()) for r in root_causes)
            cache.set("sub_causes", sub_causes)


        ancestors =  cache.get("cause_ancestors")
        if ancestors is None:
            ancestors = dict((x.pk, x.get_ancestors()) for x in models.Cause.objects.exclude(parent=None))
            cache.set("cause_ancestors", ancestors)

        for root_cause in root_causes:
            if sub_causes[root_cause.pk]:
                for sub_cause in sub_causes[root_cause.pk]:
                    cause_count = len([x for x in incidents_list if x.investigation.cause==sub_cause])
                    if cause_count > 0:
                        names = [namesc[a.pk] for a in ancestors[sub_cause.pk]] + [sub_cause.name]
                        row =  names + [""]*(max_depth -len(names)) + [cause_count]
                        chart_data.append([" &rarr; ".join(names),cause_count])
                        table.append(row)
            else:
                cause_count = len([x for x in incidents_list if x.investigation.cause==root_cause])
                if cause_count > 0:
                    names =  [root_cause.name] + [""]*(max_depth-1)
                    row =  names + [cause_count]
                    chart_data.append([" &rarr; ".join(names),cause_count])
                    table.append(row)


        header = ["Cause"]+['Sub-category']*(max_depth-1)+["Count"]
        context["cause_header"] = header
        context['cause_table'] = table
        context['cause_counts'] = json.dumps(chart_data);

    def add_domain_summary(self, context, start, end):

        incidents = models.Incident.objects.valid().filter(
            incident_date__gte=start,
            incident_date__lt=end
        ).select_related("investigation__origin_domain", "investigation__detection_domain")

        list_incidents =  list(incidents)
        max_depth = 1 + models.Domain.objects.aggregate(Max('level'))['level__max']

        table = []
        chart_data = {
            "Origin":[],
            "Detection":[]
        }

        root_domains = models.Domain.objects.filter(parent=None)

        sub_domains = cache.get("sub_domains")
        if sub_domains is None:
            sub_domains = dict((r.pk, r.get_leafnodes()) for r in root_domains)
            cache.set("sub_domains", sub_domains)

        ancestors =  cache.get("domain_ancestors")
        if ancestors is None:
            ancestors = dict((x.pk, x.get_ancestors()) for x in models.Domain.objects.all())
            cache.set("domain_ancestors", ancestors)

        for root_domain in root_domains:
            for sub_domain in sub_domains[root_domain.pk]:
                origin_count = len([x for x in incidents if x.investigation.origin_domain==sub_domain])
                detection_count = len([x for x in incidents if x.investigation.detection_domain==sub_domain])

                names = [a.name for a in ancestors[sub_domain.pk]] + [sub_domain.name]
                row =  names + [origin_count, detection_count]
                chart_data["Origin"].append([" &rarr; ".join(names),origin_count])
                chart_data["Detection"].append([" &rarr; ".join(names),detection_count])
                table.append(row)

        header = ["Domain"]+['Sub-Domain']*(max_depth-1)+["Origin Occurences", "Detection Occurences"]
        context["domain_header"] = header
        context['domain_table'] = table
        context['domain_counts'] = json.dumps(chart_data);


    def get_context_data(self):

        context = super(Statistics, self).get_context_data()
        try:
            year = int(self.request.GET.get("year", timezone.now().year))
            start = timezone.datetime(year=year, day=1, month=1)
            end = timezone.datetime(year=year+1, day=1, month=1)
        except (ValueError):
            raise Http404("Invalid year parameter")


        # the +1 day here is because django-mssql has a bug where it returns Dec 31
        # instead of Jan 1st (probably due to timezone issue) so the +1day bumps
        # it into the correct year
        years = [(d+ timezone.timedelta(days=1)).year for d in models.Incident.objects.dates("incident_date", "year")]


        context['years'] = years
        context['selected_year'] = year
        context['current_year'] = timezone.now().year

        self.add_summary_table(context, start, end)
        self.add_incident_counts(context, start, end)
        self.add_domain_summary(context, start, end)
        self.add_cause_summary(context, start, end)

        return context
