from django import template
from django.template import Context
from django.template.loader import get_template

register = template.Library()

@register.simple_tag
def kochoice(id_, name,type_, active):
    if type_ == "tree":
        template = get_template("incidents/_kotree.html")
        c = Context({"name": name, "id": id_, "active":active})
    else:
        template = get_template("incidents/_kochoice.html")
        c = Context({"name": name, "id": id_, "active":active})
    return template.render(c)
