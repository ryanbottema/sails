from django.conf.urls import patterns, include, url
import incidents.api.urls as api_urls
import views

urlpatterns = patterns('',
    url(r'^api/', include(api_urls)),

    #    url(r'^config/$', views.Config.as_view(), name="config"),
    url(r'^dashboard/$', views.Dashboard.as_view(), name="dashboard"),
    url(r'^stats/$', views.Statistics.as_view(), name="statistics"),
    url(r'^stats/incomplete/$', views.IncompleteStats.as_view(), name="incomplete-statistics"),
    url(r'^report/$', views.Report.as_view(), name="report"),


    url(r'^mine/$', views.MyIncidents.as_view(), name="my-incidents"),
    url(r'^complete/$', views.Complete.as_view(), name="complete-incidents"),
    url(r'^actual/$', views.Actual.as_view(), name="actual-incidents"),
    url(r'^invalid/$', views.Invalid.as_view(), name="invalid-incidents"),
    url(r'^complete/(?P<pk>\d+)/$', views.CompleteIncident.as_view(), name="complete-details"),

    url(r'^incomplete/$', views.Incomplete.as_view(), name="incomplete-incidents"),

    url(r'^triage/$', views.Triage.as_view(), name="triage"),

    url(r'^investigations/mine$', views.MyInvestigations.as_view(), name="my-investigations"),

    url(r'^actions/$', views.IncidentActions.as_view(), name="all-actions"),
    url(r'^actions/incomplete/$', views.IncompleteIncidentActions.as_view(), name="incomplete-actions"),
    url(r'^actions/complete/$', views.CompleteIncidentActions.as_view(), name="complete-actions"),
    url(r'^actions/mine/$', views.MyIncidentActions.as_view(), name="my-actions"),
    url(r'^actions/mine/incomplete/$', views.MyIncompleteIncidentActions.as_view(), name="my-incomplete-actions"),
    url(r'^actions/mine/complete/$', views.MyCompleteIncidentActions.as_view(), name="my-complete-actions"),

    url(r'^(?P<pk>\d+)/$', views.Incident.as_view(), name="incident"),
    url(r'^$', views.Incidents.as_view(), name="all-incidents"),
)

