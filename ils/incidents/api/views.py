from incidents.api import serializers
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

import incidents.models as models
from django.contrib.auth import get_user_model

User = get_user_model()

class IncidentViewSet(viewsets.ModelViewSet):
    queryset = models.Incident.objects.order_by('-pk')
    serializer_class = serializers.IncidentSerializer


class TriageViewSet(viewsets.ModelViewSet):
    queryset = models.Incident.triage.all()
    serializer_class = serializers.IncidentSerializer

class CompleteViewSet(viewsets.ModelViewSet):
    queryset = models.Incident.complete.all()
    serializer_class = serializers.IncidentSerializer


class StandardDescriptionViewSet(viewsets.ModelViewSet):
    queryset = models.StandardDescription.objects.all()
    serializer_class = serializers.StandardDescriptionSerializer


class IncidentActionViewSet(viewsets.ModelViewSet):
    queryset = models.IncidentAction.objects.all()
    serializer_class = serializers.IncidentActionSerializer

    def get_queryset(self):
        qs = super(IncidentActionViewSet, self).get_queryset()
        incident = self.request.QUERY_PARAMS.get('incident', None)
        if incident is not None:
            qs = qs.filter(incident__pk=incident)
        return qs


class ActionPriorityViewSet(viewsets.ModelViewSet):
    queryset = models.ActionPriority.objects.all()
    serializer_class = serializers.ActionPrioritySerializer


class ActionEscalationViewSet(viewsets.ModelViewSet):
    queryset = models.ActionEscalation.objects.all()
    serializer_class = serializers.ActionEscalationSerializer


class ActionTypeViewSet(viewsets.ModelViewSet):
    queryset = models.ActionType.objects.all()
    serializer_class = serializers.ActionTypeSerializer


class LocationViewSet(viewsets.ModelViewSet):
    queryset = models.LocationChoice.objects.all()
    serializer_class = serializers.LocationSerializer


class OperationalTypeViewSet(viewsets.ModelViewSet):
    queryset = models.OperationalType.objects.all()
    serializer_class = serializers.OperationalTypeSerializer


class TechniqueViewSet(viewsets.ModelViewSet):
    queryset = models.Technique.objects.all()
    serializer_class = serializers.TechniqueSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class TreeOrderViewSet(viewsets.ModelViewSet):
    """
    An ugly hack to get the tree_id and level
    set correctly in the ils config/tree reorder
    view.
    """
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            self.set_tree_data(request)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def set_tree_data(self, request):
        data = request.DATA
        if self.object is not None:

            for f in ("lft", "rght", "level", "tree_id"):
                setattr(self.object, f, int(data.get(f)))

            parent_url = request.DATA.get("parent")
            self.object.parent = self._model.objects.get(pk=parent_url.split("/")[-2]) if parent_url  and parent_url != "null" else None
            self.object.raw_save()





    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.set_tree_data(request)
        return super(TreeOrderViewSet, self).update(request, *args, **kwargs)



class DomainViewSet(TreeOrderViewSet):
    queryset = models.Domain.objects.all()
    serializer_class = serializers.DomainTreeOrderSerializer
    _model = models.Domain



class CauseViewSet(TreeOrderViewSet):
    queryset = models.Cause.objects.all()
    serializer_class = serializers.CauseSerializer
    _model = models.Cause

class InvestigationViewSet(viewsets.ModelViewSet):
    queryset = models.Investigation.objects.all()
    serializer_class = serializers.InvestigationSerializer













