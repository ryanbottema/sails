"use strict";


$(document).ready(function(){
    $(".datepicker").datepicker({endDate:'+0d'});
    $("#user-table").dataTable({
        bPaginate:false,
        bFilter:false
    });
});
