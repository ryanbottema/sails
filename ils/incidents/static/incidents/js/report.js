"use strict";

var all_popovers = $(".ils-popover");

function set_patient_id_text(){
    var txt = "Patient id";
    if ($("#id_incident_type").find("option:selected").val() === "clinical"){
        txt +="*";
    }
    $("label[for=patient_id]").text(txt);
}

function set_severity_choices(){

    var it = $("#id_incident_type").find("option:selected").val();
    var element = $("#id_severity");
    var cur_severity = element.find("option:selected").val();

    var severities = SEVERITIES[it] || [];
    var options = '<option value>-----</option>'+_.map(severities, function(e){return '<option value="'+e.id+'">'+e.text+'</option>';}).join("");
    var help_text;
    if (it) {
        help_text = _.map(severities, function(e){return '<strong>'+e.text+'</strong><br/>'+e.description;}).join("<br/>");
    }else{
        help_text = "Severity definitions are dependent on Incident type. Please select incident type before selecting Severity";
    }

    element.html(options);
    element.popover("destroy").parents(".ils-popover").attr("data-content", help_text);
    element.attr("placeholder", help_text);

    element.val(cur_severity).removeClass("select2-offscreen").select2({ }).on('select2-focus',function(){
        all_popovers.popover('hide');
        $(this).parents(".ils-popover").popover("show");
        if ($(this).select2("val")==="" || ($(this).select2("val") && $(this).select2("val").length===0)){
            $(this).select2("open");
        }
    });

    // reselect severity if it exists
    element.find('option[value="'+cur_severity+'"]').attr("selected","selected");
}

$(document).ready(function(){

    $(".datepicker").datepicker({endDate:'+0d'});

    $('.ils-popover').popover({
           trigger:'manual',
           animation:false,
           delay:0,
           html:true
    });

    $("label.checkbox").click(function(){
       $(this).find("input").focus();
    });

    $("input, select, textarea").on('focus',function(){
        all_popovers.popover('hide');
        $(this).parents(".ils-popover").popover("show");
    });

    $("#id_incident_type").change(function(){
        set_patient_id_text();
        set_severity_choices();
    }).trigger('change');


    $("select").addClass("input-large").select2({ }).on('select2-focus',function(){
        all_popovers.popover('hide');
        $(this).parents(".ils-popover").popover("show");
        if ($(this).select2("val")==="" || ($(this).select2("val") && $(this).select2("val").length===0)){
            $(this).select2("open");
        }
    });

    $("#report-form").submit(function(){
        $(window).unbind("beforeunload");
    });

    $(window).bind("beforeunload", function (e) {
        var form_inputs = $("#report-form").find("input, select, textarea").not("[type=hidden]").not(".select2-focusser, #id_incident_date, #id_email, #id_submitted_by, #id_auth_password");

        if (_.any(_.pluck(form_inputs,"value"))){
            var e = e || window.event;

            if (e) {
                e.returnValue = '';
            }

            return 'Are you sure you want to leave this page without submitting your incident?';
        }
    });
});
