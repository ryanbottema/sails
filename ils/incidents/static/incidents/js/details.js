"use strict";

var MIN_COMMENT_LENGTH = 8;

$(document).ready(function(){

    var s2query = "select:visible:not([readonly])";


    $('#add_more').click(function() {
        var form_idx = $('#id_form-TOTAL_FORMS').val();
        $('#actions_formset').prepend($('#empty_action_form').html().replace(/__prefix__/g, form_idx));
        $('#id_form-TOTAL_FORMS').val(parseInt(form_idx) + 1);
        $(s2query).addClass("input-xlarge").select2();
        $("a.modal-trigger").on('click', function(){
            curModal = $(this);
        });
    });

    if ($("#actions_formset input").length === 0){
        $("#add_more").trigger("click");
    }

    $(s2query).addClass("input-xlarge").select2();
    $("select:input[readonly]").addClass("input-xlarge");

    $("#edit-details, #cancel-edit").click(function(){
        $("#incident-form-wrapper, #details-display").toggle();
    });

    $("#edit-investigation, #cancel-edit-investigation").click(function(){
        $("#investigation-form-wrapper, #investigation-details-display").toggle();
    });

    $("#id_technique").change(function(e){
        var modality = TechniqueModality[$(this).val()];
        $("#id_modality").val(modality);
    });

    $("#submit-invalid, #submit-reopen").prop("disabled", true);

    $("#invalid-comment").bind('input propertychange',function(){
        var disabled = $(this).val().length < MIN_COMMENT_LENGTH;
        $("#submit-invalid").prop("disabled", disabled);
    });

    $("#reopen-comment").bind('input propertychange',function(){
        var disabled = $(this).val().length < MIN_COMMENT_LENGTH;
        $("#submit-reopen").prop("disabled", disabled);
    });

    $("a.modal-link").popover({
        trigger:"hover",
        container:"body"
    });


    $("#complete").click(function(e){
        e.preventDefault();

    });

    var curModal = null;

    $(".modal-link").on('click', function(evt){
        var pk = $(evt.target).data("pk");
        var ancestors = "";
        var txt =  '<i class="icon-question-sign"></i> ';
        if (pk) {
            ancestors = $(evt.target).siblings("input").val();
            txt = $(this).text();
        }

        curModal.html(txt);
        curModal.parent().find("input").val(pk);//select2("val",pk);
        curModal.parent().find("span.ancestors").html(ancestors);
        curModal = null;
    });

    $("a.modal-trigger").on('click', function(){
        curModal = $(this);
    });

    $("a.incident-flag").click(function(){
        var el = $(this);
        var icon = el.find("i");
        var id = el.data("incident");

        // note, I have no idea why Django.js is putting "triage" rather than "incident" in the url :(
        var url = Django.url("incident-detail", id, "json").replace("undefined","").replace("triage","incident");

        $.ajax(url, {
            method:"PATCH",
            data:{flag:!el.hasClass("flagged")},
            success: function(){
                el.toggleClass("flagged");
                $("#id_flag").prop("checked",el.hasClass("flagged"));
                icon.toggleClass("icon-flag icon-flag-alt");
            }
        });
    });

    $("#save-and-continue").click(function(){
        $("#do-save-and-continue").val(1);
    });

});
