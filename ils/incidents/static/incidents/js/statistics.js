function setupSummary(){

    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    var options = {
        series: {
            stack: 0,
            lines: {
                show: false,
                fill: true,
                steps:false
            },
            bars: {
                show: true,
                barWidth: 0.8,
                align: "center",
                lineWidth:1
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        }
    };

    var countsToFlot = function(counts){
        return _.map(counts,function(i,l){return { label:l, data:i };});
    }

    var countsToFlotHoriz = function(counts){
        return _.map(counts,function(i,l){return { label:i, data:l };});
    }

    var countData = countsToFlot(typeCounts);
    var severityData = countsToFlot(severityCounts);
    var statusData = countsToFlot(statusCounts);

    $.plot("#incident-chart", countData, _.extend({
        legend:{
            container: $("#incident-legend")
        }
    },options));

    $.plot("#severity-chart", severityData, _.extend({
        legend:{
            container: $("#severity-legend")
        }
    },options));

    $.plot("#status-chart", statusData, _.extend({
        legend:{
            container: $("#status-legend")
        }
    },options));

}

function setupDomains(){


    var nCols = $("#domain-table").last().find("thead tr th").length/2;

    var i, aoColumns=[];
    for (i=0; i < nCols-2; i++){
        aoColumns.push({type:"select"});
    }

    var dt = $("#domain-table").dataTable({
        iDisplayLength:20,
        sDom:'<<irp>><<rt>><<p>>'
    }).columnFilter({
        sPlaceHolder: "head:after",
        aoColumns:aoColumns
    });

    var originDomainData = _.map(domainCounts.Origin, function(d){return [d[1], d[0]];});
    var detectionDomainData = _.map(domainCounts.Detection, function(d){return [d[1], d[0]];});
    var domainTicks = _.map(originDomainData, function(d, i){return [i, d[1]];});

    var domainOptions = {
        series: {
            lines: {
                show: false,
                fill: true,
                steps: false
            },
            bars: {
                show: true,
                horizontal:true,
                barWidth: 0.8,
                align: "center",
                lineWidth:1
            }
        },
        yaxis: {
            mode:"categories",
            tickLength: 0,
            ticks:domainTicks
        }
    };

    $('a[href="#origin-domain-chart-tab"]').on('shown', function (e) {
        $.plot("#origin-domain-chart", [originDomainData], _.extend({},domainOptions));
        $("#origin-domain-chart .flot-y-axis .tickLabel").css("left",0);
    });

    $('a[href="#detection-domain-chart-tab"]').on('shown', function (e) {
        $.plot("#detection-domain-chart", [detectionDomainData], _.extend({},domainOptions));
        $("#detection-domain-chart .flot-y-axis .tickLabel").css("left",0);
    });

}

function setupCauses(){

    var nCols = $("#cause-table").last().find("thead tr th").length/2;

    var i, aoColumns=[];
    for (i=0; i < nCols-1; i++){
        aoColumns.push({type:"select"});
    }

    var dt = $("#cause-table").dataTable({
        iDisplayLength:20,
        sDom:'<<irp>><<rt>><<p>>'
    }).columnFilter({
        sPlaceHolder: "head:after",
        aoColumns:aoColumns
    });

    var data = _.map(causeCounts, function(d){return [d[1], d[0]];});
    var ticks = _.map(data, function(d, i){return [i, d[1]];});

    var options = {
        series: {
            lines: {
                show: false,
                fill: true,
                steps: false
            },
            bars: {
                show: true,
                horizontal:true,
                barWidth: 0.8,
                align: "center",
                lineWidth:1
            }
        },
        yaxis: {
            mode:"categories",
            tickLength: 0,
            ticks:ticks
        }
    };

    $('a[href="#cause-chart-tab"]').on('shown', function (e) {
        $.plot("#cause-chart", [data], _.extend({},options));
        $("#cause-chart .flot-y-axis .tickLabel").css("left",0);
    });


}

$(document).ready(function(){


    $("#year-select").change(function(){
        var new_year = $(this).find("option:selected").val();
        window.location = './?year='+new_year;
    });


    setupSummary();
    setupDomains();
    setupCauses();
});
