from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.forms import ModelForm, Textarea, BooleanField, DateField, CharField, PasswordInput, TextInput, DateInput, ChoiceField, ModelChoiceField, HiddenInput
from django.forms.models import modelformset_factory
from django.utils import formats

from django.utils.translation import ugettext as _

from ils.forms.widgets  import MPTTSelect
from ils.utils import get_users_by_permission

import models

User = get_user_model()

REMOVE_HELP_MESSAGE = unicode(_('Hold down "Control", or "Command" on a Mac, to select more than one.'))


class IncidentForm(ModelForm):

    actual = ChoiceField(choices=models.ACTUAL_POTENTIAL_CHOICES, label="Actual or Potential")

    class Meta:
        model = models.Incident
        fields = ('incident_date', 'actual', 'patient_id',  'incident_type', 'severity', 'location', 'description',)

        widgets = {
            "description": Textarea(attrs={"class":"input-block-level", "rows":2}),
        }


class IncidentReportForm(ModelForm):

    email = BooleanField(label="Email", help_text="Email me updates about this incident", required=False)
    auth_password = CharField(label="Password", required=True)
    submitted_by = CharField(label="Username", required=True)
    actual = ChoiceField(choices=[("","---------")]+models.ACTUAL_POTENTIAL_CHOICES, label="Actual or Potential")

    class Meta:
        model = models.Incident
        fields = ('incident_date', 'patient_id', 'actual', 'incident_type', 'severity', 'location', 'description', "email", "submitted_by", "auth_password", )

        widgets = {
                "description": Textarea(attrs={"class":"input-block-level", "rows":6}),
                "auth_password": PasswordInput(),
        }

    def __init__(self,*args,**kwargs):
        self.user = kwargs.pop("user")

        super(IncidentReportForm,self).__init__(*args,**kwargs)
        self.fields["incident_date"].input_formats = settings.DATE_INPUT_FORMATS

        self.fields["actual"].help_text = "<br/>".join(["<strong>%s</strong><br/>%s" % (x[1],models.ACTUAL_POTENTIAL_DESCRIPTIONS[x[0]]) for x in models.ACTUAL_POTENTIAL_CHOICES])
        self.fields["incident_type"].help_text = "<br/>".join(["<strong>%s</strong><br/>%s" % (x[1],models.INCIDENT_TYPE_DESCRIPTIONS[x[0]]) for x in models.INCIDENT_TYPE_CHOICES])

        for field_name in self.fields:
            field = self.fields.get(field_name)
            field.help_text = field.help_text.replace(REMOVE_HELP_MESSAGE, "")
            if field:
                field.widget.attrs["placeholder"] = field.help_text

    def clean(self):
        cleaned_data = super(IncidentReportForm, self).clean()
        submitted_by = cleaned_data.get("submitted_by")
        password = cleaned_data.get("auth_password")
        self.user =  authenticate(username=submitted_by, password=password)

        if self.user is not None:
            #replace string with actual user instance
            cleaned_data["submitted_by"] = self.user
        else:
            for n in ["submitted_by", "auth_password"]:
                if n in cleaned_data:
                    del cleaned_data[n]
                self._errors[n] = self.error_class(["Invalid Username & Password"])

        if cleaned_data.get("incident_type", None) == models.CLINICAL and not cleaned_data.get("patient_id","").strip():
            self._errors["patient_id"] = self.error_class(["Clinical incidents must include a Patient ID"])

        return cleaned_data


class Investigation(ModelForm):

    flag = BooleanField(required=False, label="Flag for discussion")
    severity = ChoiceField(choices=models.SEVERITY_CHOICES, required=False)
    intent= ChoiceField(choices=[('','---------')] + list(models.INTENT_CHOICES), required=False)
    num_affected= ChoiceField(label="# Patients Affected", choices=models.NUM_AFFECTED_CHOICES, required=False)
    standard_description = ModelChoiceField(queryset=models.StandardDescription.objects.all(), required=False)
    technique = ModelChoiceField(queryset=models.Technique.objects.all(), required=False)
    modality = CharField(required=False)

    class Meta:

        model = models.Investigation

        fields = (
            'flag',
            'investigator',
            'assigned',
            'psls_id',
            'num_affected',
            #'actual',
            'severity',
            'standard_description',
            'intent',
            'technique',
            'modality',
            'harm',
            'detection_domain',
            'origin_domain',
            'operational_type',
            'cause',
            'complete',
        )

        widgets = {
                #            "detection_domain":MPTTSelect(),
                #            "origin_domain":MPTTSelect(),
                #            "cause":MPTTSelect(),
            "complete":HiddenInput(),
            #            "origin_domain":TextInput(),
            #            "detection_domain":TextInput(),
            #            "cause":TextInput(),
        }

    def __init__(self, *args, **kwargs):

        super(Investigation, self).__init__(*args, **kwargs)

        if not self.instance.requires_domains():
            del self.fields["origin_domain"]
            del self.fields["detection_domain"]

        if not self.instance.requires_operational_type():
            del self.fields["operational_type"]


        self.fields['investigator'].queryset = get_users_by_permission("incidents.change_investigation", False).order_by("username")
        #for f in self.fields:
        #    self.fields[f].widget.attrs["class"] = "input-block-level"

        #self.fields["actual"].initial = self.instance.incident.actual
        self.fields["severity"].initial = self.instance.incident.severity
        self.fields["flag"].initial = self.instance.incident.flag
        self.fields["intent"].initial = self.instance.incident.intent
        self.fields["num_affected"].initial = self.instance.incident.num_affected
        self.fields["standard_description"].initial = self.instance.incident.standard_description
        self.fields["technique"].initial = self.instance.incident.technique
        self.fields["complete"].widget.attrs['readonly'] = "readonly"
        if self.instance.incident.technique:
            self.fields["modality"].initial = self.instance.incident.technique.modality
        else:
            self.fields["modality"].initial = ""

        self.fields["modality"].widget.attrs['readonly'] = "readonly"

        self.fields['assigned'].widget.attrs['readonly'] = 'readonly'

        if not (self.instance.incident.is_actual() and self.instance.incident.incident_type==models.CLINICAL):
            del self.fields["standard_description"]

        if (not self.instance.incident.valid):
            for field in self.fields:
                self.fields[field].widget.attrs['readonly'] = "readonly"




    def save(self, *args, **kwargs):

        super(Investigation, self).save(*args, **kwargs)

        self.instance.incident.valid = self.cleaned_data.get("valid", True)
        self.instance.incident.flag = self.cleaned_data.get("flag", False)
        #self.instance.incident.actual = self.cleaned_data.get("actual")
        self.instance.incident.severity = self.cleaned_data.get("severity")
        self.instance.incident.intent = self.cleaned_data.get("intent")
        self.instance.incident.num_affected= self.cleaned_data.get("num_affected")
        self.instance.incident.technique = self.cleaned_data.get("technique")
        self.instance.incident.standard_description = self.cleaned_data.get("standard_description")

        if "commit" not in kwargs or kwargs["commit"]:
            self.instance.incident.save()

        return self.instance


class IncidentActionForm(ModelForm):

    class Meta:
        model = models.IncidentAction
        fields=("action_type", "priority", "escalation", "responsible", "description", "complete",)

    def __init__(self, *args, **kwargs):

        super(IncidentActionForm, self).__init__(*args, **kwargs)

        self.fields['responsible'].queryset = get_users_by_permission("incidents.change_investigation", False).order_by("username")

        for name, f in self.fields.items():
            f.widget.attrs['class'] = 'input-xlarge'
            if self.instance.complete and name != 'complete':
                f.widget.attrs['readonly'] = 'readonly'

            if isinstance(f.widget, Textarea):
                f.widget.attrs['class'] = 'input-block-level'
                f.widget.attrs['rows'] = 3


IncidentActionFormSet = modelformset_factory( models.IncidentAction, form=IncidentActionForm, extra=0)

