from django.db import models
from django.db.models import Q
from django.core.urlresolvers import reverse
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.utils.safestring import mark_safe
from mptt.models import MPTTModel, TreeForeignKey
from paintstore.fields import ColorPickerField


PALLIATIVE  = "palliative"
CURATIVE = "curative"
ADJUVANT = "adjuvant"
NEOADJUVANT = "neoadjuvant"
UNKNOWN = "unknown"

CLINICAL = "clinical"
OCCUPATIONAL = "occupational"
OPERATIONAL = "operational"
ENVIRONMENTAL = "environmental"
SECURITY = "security"

INCIDENT_TYPE_CHOICES = (
    (CLINICAL, _("Clinical")),
    (OCCUPATIONAL, _("Occupational")),
    (OPERATIONAL, _("Operational")),
    (ENVIRONMENTAL, _("Environmental")),
    (SECURITY, _("Security")),
)

INCIDENT_TYPE_DESCRIPTIONS = {
    CLINICAL:"An incident relating to patient safety, or treatment-related processes. Examples of clinical incidents include radiation misadministration, error in an individual patient treatment or treatment plan.",
    OCCUPATIONAL:"An incident relating staff, student and visiting worker safety.",
    OPERATIONAL: "An incident involving operational and technical systems related to machines, equipment, facilities, operational capability, procedures, patient flow, and staff scheduling, etc.",
    ENVIRONMENTAL: "An incident involving processes preventing environmental exposure to radiation, drugs or chemicals.",
    SECURITY: "An incident relating to personal and public security, information security, system integrity, physical asset security, and public image, etc.",
}


PHOTONS = "photons"
ELECTRONS = "electrons"
BRACHY = "brachy"
ORTHO = "ortho"
NA = "n/a"

MODALITY_CHOICES = (
    (NA,"N/A"),
    (PHOTONS, "Photons"),
    (ELECTRONS, "Electrons"),
    (BRACHY, "Brachy"),
    (ORTHO, "Ortho"),
)

INTENT_CHOICES = (
    (PALLIATIVE, _("Palliative")),
    (CURATIVE, _("Curative")),
    (ADJUVANT, _("Adjuvant")),
    (NEOADJUVANT, _("Neoadjuvant")),
    (UNKNOWN, _("Unknown")),
    (NA, _("N/A")),
)

ACTUAL = "actual"
POTENTIAL = "potential"


ACTUAL_POTENTIAL_CHOICES = [
    (ACTUAL, _("Actual")),
    (POTENTIAL, _("Potential")),
]


ACTUAL_POTENTIAL_DESCRIPTIONS = {
    ACTUAL:_("An actual incident"),
    POTENTIAL:_("A potential or near miss incident"),
}

SEVERITY_CHOICES = (
    ("critical", _("Critical")),
    ("serious", _("Serious")),
    ("major", _("Major")),
    ("minor", _("Minor")),
)


DOH_UNKNOWN= "unknown"
DOH_NONE = "none"
DOH_MILD = "mild"
DOH_MODERATE = "moderate"
DOH_SEVERE = "severe"
DOH_DEATH = "death"
DOH_NA = NA

DOH_CHOICES = (
    (DOH_UNKNOWN, _("Unknown")),
    (DOH_NONE, _("None")),
    (DOH_MILD, _("Mild")),
    (DOH_MODERATE, _("Moderate")),
    (DOH_SEVERE, _("Severe")),
    (DOH_DEATH, _("Death")),
    (DOH_NA, _("N/A")),
)

DOH_DESCRIPTIONS = {
    DOH_UNKNOWN:_("Unknown"),
    DOH_NONE:_("Patient outcome is not symptomatic or no symptoms detected and no treatment is required."),
    DOH_MILD:_("Patient outcome is symptomatic, symptoms are mild, loss of function or harm is minimal or intermediate but short term, and no or minimal intervention (extra observation, investigation) is required."),
    DOH_MODERATE:_("Patient outcome is symptomatic, requiring intervention, (additional operative procedure, additional therapeutic treatment), an increased length of stay, or causing permanent or long term harm or loss of function."),
    DOH_SEVERE:_("Patient outcome is symptomatic, requiring life-saving intervention or major surgical/medical intervention, shortening life expectancy or causing major permanent or long term harm or loss of function."),
    DOH_DEATH:_("On balance of probabilities, death was caused or brought forward in the short term by the incident."),
    DOH_NA:_("N/A"),
}

NONE = "none"
ONE = "one"
MORE_THAN_ONE = "morethanone"

NUM_AFFECTED_CHOICES = (
    ("","---------"),
    (NONE, _("None")),
    (ONE, _("One")),
    (MORE_THAN_ONE, _("More Than One")),
)


AFFECTED_CHOICES = [_("No One Affected"), _("One Patient Affected"), _("Several Patients Affected (Give #_)"), _("Staff"), _("Other (specify)"), _("Unknown")]
AFFECTED_CHOICES = zip(range(len(AFFECTED_CHOICES)),AFFECTED_CHOICES)
TYPE_CHOICES = [(1,_("Actual Incident")), (2, _("Near-miss; no-one affected"))]
#MODALITY_CHOICES = [_("Photons"), _("Electrons"), _("Particles"), _("Brachytherapy"), _("Radioisotope"), _("Other (specify)"), _("Not applicable")]
#MODALITY_CHOICES = zip(range(1,len(MODALITY_CHOICES)+1), MODALITY_CHOICES)
STAFF_ROLE_CHOICES = [_("Attending Rad. Onc."), _("Resident Rad. Onc."), _("Other Physician"), _("Radiation Therapist"), _("Dosimetrist"), _("Physicist"), _("Nurse, NP or PA"), _("Administrator"), _("Patient"), _("Other (specify)")]
STAFF_ROLE_CHOICES = zip(range(1, len(STAFF_ROLE_CHOICES)+1), STAFF_ROLE_CHOICES)

WEAKER, INTERMEDIATE, STRONGER = "weaker", "intermediate", "stronger"
STRENGTH_CHOICES = ( (WEAKER, _("Weaker")),(INTERMEDIATE, _("Intermediate")),(STRONGER, _("Stronger")),)

class MPTTModelMixin(object):

    def ancestor_names(self):
        ancestors = list(self.get_ancestors(include_self=True).values_list("name", flat=True))
        return mark_safe("&rarr;".join(ancestors ))

    def ancestor_names_plain(self):
        ancestors = list(self.get_ancestors(include_self=True).values_list("name", flat=True))
        return " -> ".join(ancestors)

    def raw_save(self, *args, **kwargs):
        super(MPTTModel, self).save(*args, **kwargs)


class AbstractChoice(models.Model):

    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True, help_text=_("URL friendly version of name (only a-Z, 0-9 and _ allowed)"))
    description = models.TextField(help_text=_("Concise description of this incident type"), null=True, blank=True)
    color = ColorPickerField(max_length=255, null=True, blank=True)
    order = models.PositiveIntegerField(help_text=_("Order in which this Incident Type will be displayed"))

    class Meta:
        abstract = True
        ordering = ('order',)


    def __unicode__(self):
        return self.name


class IncidentManager(models.Manager):
    def valid(self):
        return self.get_query_set().filter(valid=True)


class CompleteIncidentManager(models.Manager):
    def get_queryset(self):
        return super(CompleteIncidentManager, self).get_queryset().filter(valid=True, investigation__complete=True)


class ActualIncidentManager(models.Manager):
    def get_queryset(self):
        return super(ActualIncidentManager, self).get_queryset().filter(valid=True, actual=ACTUAL)


class IncompleteIncidentManager(models.Manager):
    def get_queryset(self):
        return super(IncompleteIncidentManager, self).get_queryset().filter(valid=True, investigation__complete=False).exclude(investigation__investigator=None)


class IncompleteOrFlaggedIncidentManager(models.Manager):

    def get_queryset(self):
        qs = super(IncompleteOrFlaggedIncidentManager, self).get_queryset()
        incomplete = Q(valid=True, investigation__complete=False)
        flagged = Q(flag=True)

        qs = qs.filter(incomplete | flagged).exclude(investigation__investigator=None)
        return qs

class TriageIncidentManager(models.Manager):
    def get_queryset(self):
        return super(TriageIncidentManager, self).get_queryset().filter(valid=True, investigation__investigator=None)


class StandardDescription(AbstractChoice):
    pass


class Technique(AbstractChoice):
    modality = models.CharField(max_length=15, choices=MODALITY_CHOICES)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.get_modality_display())


class Incident(models.Model):

    incident_date = models.DateField(help_text=_("Date that the incident occured"))
    patient_id = models.CharField(max_length=255, help_text=_("Patient ID (where applicable)"), null=True, blank=True)
    incident_type = models.CharField(max_length=15, help_text=_("Incident Type"), choices=INCIDENT_TYPE_CHOICES )
    severity = models.CharField(max_length=15, choices=SEVERITY_CHOICES, null=True, blank=True)
    intent = models.CharField(max_length=15, help_text=_("What was the intent of the treatment?"), choices=INTENT_CHOICES, null=True, blank=True)
    technique = models.ForeignKey(Technique, help_text=_("What treatment technique was involved?"), null=True, blank=True)
    num_affected = models.CharField(verbose_name=_("# Patients Affected"), max_length=15, help_text=_("How many patients were affected?"), choices=NUM_AFFECTED_CHOICES, null=True, blank=True)

    location = models.ForeignKey("LocationChoice", help_text=_("Location where the incident was discovered."))
    description = models.TextField(help_text=_("Briefly summarize the incident"))

    submitted = models.DateTimeField(auto_now_add=True)
    submitted_by = models.ForeignKey(settings.AUTH_USER_MODEL)

    actual = models.CharField(max_length=255,  help_text=_("Was this an actual incident or a potential/near miss incident"), choices=ACTUAL_POTENTIAL_CHOICES)
    standard_description = models.ForeignKey(StandardDescription, null=True, blank=True)

    valid = models.BooleanField(default=True)

    complete_fields = ["incident_date", "incident_type", "severity", "intent", "technique", "location", "actual", "num_affected"]

    flag = models.BooleanField(verbose_name=_("Flag for discussion"), default=False)

    objects = IncidentManager()
    complete = CompleteIncidentManager()
    actuals = ActualIncidentManager()
    triage = TriageIncidentManager()
    incomplete = IncompleteIncidentManager()
    incomplete_or_flagged = IncompleteOrFlaggedIncidentManager()

    class Meta:
        ordering = ("-submitted",)

    def missing_fields(self):
        missing = []
        for f in self.complete_fields:
            attr = getattr(self,f)
            try:
                val = attr.all()
                if len(val) == 0:
                    missing.append(f)
            except AttributeError:
                if attr is None or attr == "":
                    missing.append(f)
        return missing


    def complete_fields_set(self):
        return len(self.missing_fields()) == 0

    def needs_standard_description(self):
        return self.is_actual() and self.incident_type == CLINICAL

    def save(self, *args, **kwargs):

        std_description_set = self.standard_description is not None
        if not self.needs_standard_description() and std_description_set:
            self.standard_description = None

        super(Incident,self).save(*args, **kwargs)

        try:
            self.investigation
        except Investigation.DoesNotExist:
            self.investigation = Investigation.objects.create(incident=self)

        if not self.valid:
            self.investigation.complete=True
            self.investigation.save()

        domain_set = self.investigation.origin_domain is not None or self.investigation.detection_domain is not None
        if self.incident_type != CLINICAL and domain_set:
            self.investigation.incident_domain = None
            self.investigation.detection_domain =None
            self.investigation.save()


        op_type_set = self.investigation.operational_type is not None
        if self.incident_type != OPERATIONAL and op_type_set:
            self.investigation.operational_type = None
            self.investigation.save()

    def is_actual(self):
        return self.actual == ACTUAL

    def is_potential(self):
        return self.actual == POTENTIAL

    def actual_display(self):
        return dict(ACTUAL_POTENTIAL_CHOICES)[self.actual] if self.actual else ""

    def get_absolute_url(self):
        return reverse('incident', kwargs={"pk":self.pk})

    def __unicode__(self):
        return "Incident(%d, %s, %s)" %(self.pk, self.incident_date, self.actual_display())


class Cause(MPTTModelMixin, MPTTModel):

    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True, help_text=_("A short description of this cause type. (Optional)"))
    parent =  TreeForeignKey('self', null=True, blank=True, related_name="children", help_text=_("If this is a sub-classification choose the causes parent class"))


    def __unicode__(self):
        return self.name


class ActionType(AbstractChoice):

    strength = models.CharField(max_length=15, choices=STRENGTH_CHOICES, null=True, blank=True)

    class Meta(AbstractChoice.Meta):
        verbose_name = _("Learning Action Type")
        verbose_name_plural = _("Learning Action Types")

    def strength_display(self):
        return dict(STRENGTH_CHOICES)[self.strength] if self.strength else ""

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.strength_display())


class OperationalType(AbstractChoice):

    class Meta(AbstractChoice.Meta):
        verbose_name = _("Operational Incident Type")
        verbose_name_plural = _("Operational Incident Types")


class IncidentAction(models.Model):

    class Meta:
        verbose_name_plural = _("Learning Actions")

    incident = models.ForeignKey(Incident)
    action_type = models.ForeignKey(ActionType)
    description = models.TextField(verbose_name=_("Task Description"))
    responsible = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="incidentactions")

    priority = models.ForeignKey("ActionPriority", null=True, blank=True)
    escalation = models.ForeignKey("ActionEscalation", null=True, blank=True)

    assigned = models.DateTimeField(auto_now_add=True)
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="incidentactions_assigned")

    complete = models.BooleanField(default=False)
    completed = models.DateTimeField(null=True, blank=True)


class InvestigationManager(models.Manager):

    def incomplete(self):
        return self.get_query_set().filter(complete=False)

    def user_all(self, user):
        return self.get_query_set().filter(investigator=user)

    def user_incomplete(self, user):
        return self.user_all(user).filter(complete=False)


class Investigation(models.Model):

    incident = models.OneToOneField(Incident)

    investigator = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="investigations", null=True, blank=True)
    assigned = models.DateTimeField(verbose_name=_("Date Investigator Assigned"), null=True, blank=True)
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="incidents_assigned", null=True, blank=True)

    detection_domain = models.ForeignKey("Domain", verbose_name=_("Detection - Domain"), null=True, blank=True, related_name="dection_incidents")
    origin_domain = models.ForeignKey("Domain",verbose_name=_("Origin - Domain"), null=True, blank=True, related_name="origin_incidents")

    operational_type = models.ForeignKey("OperationalType",verbose_name=_("Operational Incident Type"), null=True, blank=True)

    harm = models.CharField(max_length=15, verbose_name=_("Degree of Harm"), null=True, blank=True, choices=DOH_CHOICES)

    psls_id = models.CharField(max_length=20,help_text=_("Hospital ILS #"), null=True, blank=True)

    cause = models.ForeignKey("Cause", null=True, blank=True)

    complete = models.BooleanField(default=False )
    completed = models.DateTimeField(null=True, blank=True)

    objects = InvestigationManager()

    complete_fields = ["investigator", "cause", "harm" ]

    def requires_domains(self):
        return self.incident.incident_type == CLINICAL

    def requires_operational_type(self):
        return self.incident.incident_type == OPERATIONAL

    def missing_fields(self):
        inv_fields = [f for f in self.complete_fields if not getattr(self,f)]
        inc_fields = self.incident.missing_fields()
        if self.incident.is_actual():
            if self.psls_id  in ("", None):
                inv_fields.append("psls_id")
            if self.incident.standard_description  is None and self.incident.incident_type == CLINICAL:
                inc_fields.append("standard_description")

        if self.incident.incident_type == OPERATIONAL:
            if self.operational_type is None:
                inv_fields.append("operational_type")
        elif self.incident.incident_type == CLINICAL:
            if self.origin_domain is None:
                inv_fields.append("origin_domain")
            if self.detection_domain is None:
                inv_fields.append("detection_domain")

        inc_fields = ["incident__%s" % f for f in inc_fields]
        return inv_fields + inc_fields

    def required_complete(self):

        if not self.incident.valid:
            return True

        has_required_fields = self.complete_fields_set() and self.incident.complete_fields_set()

        if self.incident.is_actual():
            return has_required_fields and self.actual_fields_complete()

        return has_required_fields

    def complete_fields_set(self):
        return all([getattr(self,f) for f in self.complete_fields])

    def actual_fields_complete(self):
        has_psls_id = self.psls_id is not None and len(self.psls_id.strip()) > 0
        std_desc_required = self.incident.incident_type == CLINICAL
        if not std_desc_required:
            return has_psls_id

        has_std_desc = self.incident.standard_description is not None
        return has_psls_id and has_std_desc


@receiver(post_delete, sender=Investigation)
def post_delete_investigation(sender, instance, *args, **kwargs):
    if instance.incident: # just in case user is not specified
        instance.incident.delete()


class Domain(MPTTModelMixin, MPTTModel):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True, help_text=_("A short description of this cause type. (Optional)"))
    parent =  TreeForeignKey('self', null=True, blank=True, related_name="children", help_text=_("If this is a sub-classification choose the causes parent class"))

    def __unicode__(self):
        return self.name


class LocationChoice(AbstractChoice):
    pass


class SeverityDefinition(models.Model):
    incident_type = models.CharField(max_length=15, help_text=_("Incident Type"), choices=INCIDENT_TYPE_CHOICES )
    severity = models.CharField(max_length=15, help_text=_("Incident Severity"), choices=SEVERITY_CHOICES )
    definition = models.TextField(help_text=_("A short description of this incident type/severity"))

    class Meta:
        ordering =("incident_type", "severity",)

    def display_html(self):
        return mark_safe("%s &rarr; %s"%(self.get_incident_type_display(), self.get_severity_display()))

    def __unicode__(self):
        return "%s : %s"%(self.get_incident_type_display(), self.get_severity_display())


class ActionPriority(AbstractChoice):
    pass


class ActionEscalation(AbstractChoice):
    pass



# cache invalidation
@receiver(post_delete, sender=SeverityDefinition)
@receiver(post_save, sender=SeverityDefinition)
def severity_def_change(sender, instance, *args, **kwargs):
    cache.delete(make_template_fragment_key('severity_modal'))


@receiver(post_delete, sender=Domain)
@receiver(post_save, sender=Domain)
def domain_def_change(sender, instance, *args, **kwargs):
    cache.delete(make_template_fragment_key('domain_modal'))
    for key in settings.DOMAIN_CACHE_KEYS:
        cache.delete(key)


@receiver(post_delete, sender=Cause)
@receiver(post_save, sender=Cause)
def cause_def_change(sender, instance, *args, **kwargs):
    cache.delete(make_template_fragment_key('cause_modal'))
    for key in settings.CAUSE_CACHE_KEYS:
        cache.delete(key)


@receiver(post_delete, sender=ActionType)
@receiver(post_save, sender=ActionType)
def action_def_change(sender, instance, *args, **kwargs):
    cache.delete(make_template_fragment_key('action_modal'))

