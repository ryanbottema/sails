from django.contrib import admin
from django.contrib.admin import DateFieldListFilter
from mptt.admin import MPTTModelAdmin
from django_mptt_admin.admin import DjangoMpttAdmin
from adminsortable.admin import SortableAdminMixin
from import_export.admin import ExportMixin

from . import models, resources

class MPTTModelAdmin(DjangoMpttAdmin):
    list_display = ("name", "parent", "lft", "rght", "level", "tree_id",)


class NameSlugAdminMixin(object):
    prepopulated_fields = {"slug": ("name",)}


class NameSlugAdmin(NameSlugAdminMixin, SortableAdminMixin, admin.ModelAdmin):
    pass


def short_description(inc):
    des = inc.description[:50]
    if len(inc.description) > 50:
        des += "..."
    return des

class IncidentExportAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentResource
    list_filter = (
        ("incident_date", DateFieldListFilter),
        "actual",
        "technique",
        "severity",
        "intent",
        "location",
    )
    list_display = (
        "__unicode__",
        "incident_date",
        "actual",
        "severity",
        short_description,
    )



class InvestigationExportAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentResource

    search_fields = ("psls_id",)

    list_filter = (
        "complete",
        "operational_type",
        "harm"
    )

    list_display = (
        "incident",
        "complete",
        "psls_id",
        "operational_type",
        "origin_domain",
        "detection_domain",
        "harm"

    )


class LearningActionAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = resources.IncidentActionResource

    list_display = ("incident", "action_type", "responsible", "priority", "escalation",)
    list_filter = ("priority", "escalation",)

admin.site.register([
        models.LocationChoice,
        models.StandardDescription, models.ActionEscalation, models.ActionPriority,
        models.Technique, models.OperationalType
    ],
    NameSlugAdmin
)

admin.site.register([models.IncidentAction], LearningActionAdmin)
admin.site.register([models.SeverityDefinition], admin.ModelAdmin)
admin.site.register([models.Incident], IncidentExportAdmin)
admin.site.register([models.Investigation], InvestigationExportAdmin)
admin.site.register([models.Cause, models.Domain], MPTTModelAdmin)
admin.site.register([models.ActionType], NameSlugAdmin)
