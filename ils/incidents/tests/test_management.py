from django.test import TestCase
from .. import models, forms
import incidents.management.commands.makeinc as makeinc

from django.conf import settings
from django.contrib.auth.models import  AnonymousUser
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.urlresolvers import reverse
from django.forms import ValidationError
from django.http import Http404
from django.test.client import Client

import django.utils.timezone as timezone
from faker import Faker
import random

faker = Faker()


class TestReportForm(TestCase):
    fixtures = [
        'domains.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def test_incidents_created(self):
        makeinc.Command().handle(2)
        self.assertEqual(len(models.Incident.objects.all()),2)

    def test_no_arg(self):
        makeinc.Command().handle()
        self.assertTrue(len(models.Incident.objects.all())==0)

