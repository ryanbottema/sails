"""Production settings and globals."""

from os import environ

from base import *

#database_settings contains database user/password information that should not go in version control!
from database_settings import *

#active_directory_settings contains database user/password information that should not go in version control!
from active_directory_settings import *
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'accounts.backends.ActiveDirectoryGroupMembershipSSLBackend',
)

#email_settings contains database user/password information that should not go in version control!
from email_settings import *


DEBUG=True
DEFAULT_GROUP_NAMES = []
FORCE_SCRIPT_NAME = "/sails"
LOGIN_REDIRECT_URL = "/sails/report"
LOGIN_URL = "/sails/login"
STATIC_URL = '/sails_static/'


ADMINS = (
    ('Crystal Angers', 'cangers@toh.on.ca'),
)
MANAGERS = ADMINS

