from django.conf.urls import patterns, include, url
import incidents.api.urls as api_urls
import views

urlpatterns = patterns('',
    url(r'^unsubscribe/(?P<pk>\d+)/$', views.Unsubscribe.as_view(), name="unsubscribe"),
    url(r'^subscribe/(?P<pk>\d+)/$', views.Subscribe.as_view(), name="subscribe"),
)

