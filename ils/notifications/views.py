from django.shortcuts import render
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.views.generic import DeleteView, View

from . import models


class Subscribe(View):

    template_name_suffix = "_create"
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        models.Subscription.objects.get_or_create(user=self.request.user, incident_id=pk)
        messages.success(request, "You are now subscribed to incident %s" % pk)
        return HttpResponseRedirect(reverse("incident", kwargs={"pk":pk}))


class Unsubscribe(DeleteView):

    model = models.Subscription

    def get_object(self, *args, **kwargs):

        pk = self.kwargs["pk"]
        try:
            obj = models.Subscription.objects.get(incident=pk, user=self.request.user)
        except models.Subscription.DoesNotExist:
            raise Http404("You are not subscribed to this incident!")

        return obj

    def get_success_url(self):

        if "cancel" not in self.request.POST:
            messages.success(self.request, "Successfully unsubscribed from Incident #%d" % (self.object.incident.pk))

        if self.request.POST.get("next", None) == "incident":
            return reverse("incident", kwargs={"pk":self.kwargs['pk']})
        elif self.request.user.has_module_perms("incidents"):
            return reverse("dashboard")

        return reverse("report")

    def post(self, request, *args, **kwargs):

        if "cancel" in request.POST:
            return HttpResponseRedirect(self.get_success_url())

        return super(Unsubscribe, self).post(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(Unsubscribe, self).get_context_data(*args, **kwargs)
        context["next"] = self.request.GET.get("next", None)
        return context


